package com.avalonhcs.claimeditor.integration;


import com.avalonhcs.claimeditor.ClaimEditorWorkflow;
import com.avalonhcs.claimeditor.export.ResultsProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClaimController {

    @Autowired
    ClaimEditorWorkflow claimEditorWorkflow;

    @GetMapping("/claim")
    public String editClaim(@RequestParam("claim_number") String claimNumber) {
        return claimEditorWorkflow.editClaim(claimNumber);
    }

}
