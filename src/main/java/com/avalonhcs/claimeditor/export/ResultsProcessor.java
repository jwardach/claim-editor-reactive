package com.avalonhcs.claimeditor.export;


import io.reactivex.Observable;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class ResultsProcessor {

    PublishProcessor<String> resultsStream;

    @PostConstruct
    public void initialize() {
        resultsStream = PublishProcessor.create();
        resultsStream
                .observeOn(Schedulers.newThread())
                .subscribe(i -> {
                    Thread.sleep(2000);
                    System.out.println("Sending edited claim " + i + " to Queue on: " +Thread.currentThread().getName());
                },
                e-> {
                    System.out.println(e);
                });
    }

    public void addToResultsStream(final String results) {
        resultsStream.onNext(results);
    }
}
