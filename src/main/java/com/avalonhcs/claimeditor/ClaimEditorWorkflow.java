package com.avalonhcs.claimeditor;


import com.avalonhcs.claimeditor.export.ResultsProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClaimEditorWorkflow {

    @Autowired
    ResultsProcessor resultsProcessor;

    public String editClaim(final String claim) {
        /*
        Run rules
         */
        resultsProcessor.addToResultsStream(claim);
        System.out.println("Claim number " + claim + " edited on: " + Thread.currentThread().getName());
        return claim;
    }


}
