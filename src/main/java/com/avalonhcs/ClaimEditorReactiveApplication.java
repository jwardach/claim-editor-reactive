package com.avalonhcs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClaimEditorReactiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClaimEditorReactiveApplication.class, args);
	}

}
